//
//  print_mcp.m
//  Ultraconserved
//
//  Created by Scott Christley on 9/7/07.
//  Copyright 2007 Scott Christley. All rights reserved.
//

#import <BioCocoa/BCFoundation.h>

void print_usage(NSArray *args);

#define FASTA_FORMAT 1
#define SUMMARY_FORMAT 2
#define TABLE_FORMAT 3
#define RAW_FORMAT 4

int main(int argc, char *argv[])
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  NSArray *args = [[NSProcessInfo processInfo] arguments];

  if (([args count] != 3) && ([args count] != 4)) {
    print_usage(args);
    return 1;
  }

  int outputFormat = 0;
  if ([[args objectAtIndex: 1] caseInsensitiveCompare: @"F"] == NSOrderedSame)
    outputFormat = FASTA_FORMAT;
  else if ([[args objectAtIndex: 1] caseInsensitiveCompare: @"S"] == NSOrderedSame)
    outputFormat = SUMMARY_FORMAT;
  else if ([[args objectAtIndex: 1] caseInsensitiveCompare: @"T"] == NSOrderedSame)
    outputFormat = TABLE_FORMAT;
  else if ([[args objectAtIndex: 1] caseInsensitiveCompare: @"R"] == NSOrderedSame)
    outputFormat = RAW_FORMAT;
  else {
    printf("\nERROR: invalid argument, specify S, F or T for summary(S), fasta(F) or table(T) format.\n");
    print_usage(args);
    return 1;
  }

  int minLength = 0;
  if ([args count] == 4) minLength = [[args objectAtIndex: 3] intValue];

  NSString *mcpFile = [args objectAtIndex: 2];
  BCMCP *anMCP = [[BCMCP alloc] initWithContentsOfFile: mcpFile inMemory: NO];
  if (!anMCP) {
    NSLog(@"Failed to load MCP file: %@\n", mcpFile);
    return 1;
  }
  
  switch (outputFormat) {
    case SUMMARY_FORMAT:
      [anMCP summaryFormatWithMinimumLength: minLength];
      break;
    case FASTA_FORMAT:
      [anMCP fastaFormatWithMinimumLength: minLength];
      break;
    case TABLE_FORMAT:
      [anMCP tableFormatWithMinimumLength: minLength];
      break;
    case RAW_FORMAT:
      break;
  }

  [pool release];
  return 0;
}

void print_usage(NSArray *args)
{
  NSString *s = [[args objectAtIndex: 0] lastPathComponent];

  printf("\nPrint maximal common prefixes.\n\n");
  printf("Usage:\n");
  printf("%s S|F|T input_file [min_length]\n\n", [s UTF8String]);
  printf("S|F|T      : print in summary(S), fasta(F), or table(T) format.\n");
  printf("input_file : mcp file, assumes .mcp extension.\n");
  printf("min_length : print all mcp with greater or equal length [OPTIONAL].\n");
  printf("\nExample (prints the maximum common prefix)\n");
  printf("%s s human1_mouse1\n", [s UTF8String]);
  printf("\nExample (prints all maximum common prefixes in fasta format)\n");
  printf("%s f human1_mouse1 1\n", [s UTF8String]);
  printf("\n");
}
