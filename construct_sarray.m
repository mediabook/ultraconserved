//
//  construct_sarray.m
//  Ultraconserved tools in BioCocoa
//
//  Created by Scott Christley on 9/7/07.
//  Copyright 2007 Scott Christley. All rights reserved.
//
//  This code is covered by the Creative Commons Share-Alike Attribution license.
//	You are free:
//	to copy, distribute, display, and perform the work
//	to make derivative works
//	to make commercial use of the work
//
//	Under the following conditions:
//	You must attribute the work in the manner specified by the author or licensor.
//	If you alter, transform, or build upon this work, you may distribute the
//      resulting work only under a license identical to this one.
//
//	For any reuse or distribution, you must make clear to others
//      the license terms of this work.
//	Any of these conditions can be waived if you get permission from the copyright holder.
//
//  For more info see: http://creativecommons.org/licenses/by-sa/2.5/
//

#import <BioCocoa/BCFoundation.h>

void print_usage(NSArray *args);

int main(int argc, char *argv[])
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  NSArray *args = [[NSProcessInfo processInfo] arguments];
  int i = 1;

  if (([args count] != 3) && ([args count] != 4) && ([args count] != 5)) {
    print_usage(args);
    return 1;
  }

  BOOL softMask = NO;
  if ([[args objectAtIndex: 1] caseInsensitiveCompare: @"-softMask"] == NSOrderedSame) {
    softMask = YES;
    if ([args count] == 3) {
      print_usage(args);
      return 1;
    }
    i = 2;
  }

  // do only one strand?
  NSString *strand = nil;
  if (((i == 2) && ([args count] == 5)) || ((i == 1) && ([args count] == 4))) {
    if ([[args objectAtIndex: (i + 2)] caseInsensitiveCompare: @"R"] == NSOrderedSame)
      strand = @"R";
    else if ([[args objectAtIndex: (i + 2)] caseInsensitiveCompare: @"F"] == NSOrderedSame)
      strand = @"F";
    else {
      print_usage(args);
      return 1;
    }
  }

  NSString *inFile = [args objectAtIndex: i];
  NSString *outFile = [args objectAtIndex: (i + 1)];

  printf("Constructing suffix array.\n");
  BCSuffixArray *suffixArray = [[BCSuffixArray alloc] init];
  [suffixArray setSoftMask: softMask];
  if (![suffixArray constructFromSequenceFile: inFile strand: strand]) {
    NSLog(@"Failed to construct suffix array.\n");
    return 1;
  }

  printf("Saving suffix array.\n");
  if (![suffixArray writeToFile: outFile withMasking: YES]) {
    NSLog(@"Failed to save suffix array to file: %@\n", outFile);
    return 1;
  }

  [pool release];

  return 0;
}

void print_usage(NSArray *args)
{
  NSString *s = [[args objectAtIndex: 0] lastPathComponent];

  printf("\nConstruct suffix array.\n\n");
  printf("Usage:\n");
  printf("%s [-softMask] input_file output_file [f|r]\n\n", [s UTF8String]);
  printf("-softMask : optional flag to mask lower case letters, default is no masking.\n");
  printf("input_file : input sequence file.\n");
  printf("output_file : output suffix array file, .sa extension will be added.\n");
  printf("\nThe optional parameter is to construct the suffix array on just\n");
  printf("one strand of the sequence, the default is both strands.\n");
  printf("F|R : forward or reverse strand only.\n");
  printf("\nExample\n");
  printf("%s human_chr1.fa human_chr1\n", [s UTF8String]);
  printf("\n");

  printf("\nReference:\n");
  printf("Scott Christley, Neil F. Lobo and Greg Madey.\n");
  printf("Multiple Organism Algorithm for Finding Ultraconserved Elements.\n");
  printf("BMC Bioinformatics, 9: 15, 2008.\n\n");
}
