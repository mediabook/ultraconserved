//
//  print_sarray.m
//  Ultraconserved
//
//  Created by Scott Christley on 9/24/07.
//  Copyright 2007 Scott Christley. All rights reserved.
//

#import <BioCocoa/BCFoundation.h>

void print_usage(NSArray *args);

int main(int argc, char *argv[])
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  NSArray *args = [[NSProcessInfo processInfo] arguments];

  if (([args count] != 2) && ([args count] != 6)) {
    print_usage(args);
    return 1;
  }

  // open suffix array
  NSString *saFile = [args objectAtIndex: 1];
  BCSuffixArray *anArray = [[BCSuffixArray alloc] initWithContentsOfFile: saFile inMemory: NO];
  if (!anArray) {
    NSLog(@"Failed to load suffix array file: %@\n", saFile);
    return 1;
  }

  if ([args count] == 2) {
    [anArray dumpSuffixArray];
  } else {
    int aSeq = [[args objectAtIndex: 2] intValue];
    int aPos = [[args objectAtIndex: 4] intValue];
    int aLen = [[args objectAtIndex: 5] intValue];
    if ([[args objectAtIndex: 3] caseInsensitiveCompare: @"R"] == NSOrderedSame) {
      BCSequenceArray *seqArray = [anArray sequenceArray];
      BCSequence *theSeq = [seqArray sequenceAtIndex: aSeq];
      aPos += [theSeq length];
    }
    [anArray dumpSuffixArrayForSequence: aSeq position: aPos length: aLen];
  }

  [pool release];
  return 0;
}

void print_usage(NSArray *args)
{
  NSString *s = [[args objectAtIndex: 0] lastPathComponent];

  printf("\nPrint suffix array.\n\n");
  printf("Usage:\n");
  printf("%s input_file [seq# F|R offset length]\n\n", [s UTF8String]);
  printf("input_file : suffix array file, assumes .sa extension.\n");
  printf("\nThe optional parameters will search through the suffix array\n");
  printf("for the specific sequence and position, and output sequence data\n");
  printf("upto a specified length.\n\n");
  printf("seq# : sequence number if multple sequences, starts with 0.\n");
  printf("F|R : forward or reverse strand.\n");
  printf("offset : offset in sequence, the start of the suffix string.\n");
  printf("length : length of sequence data to output.\n");
  printf("\nExample (prints the complete suffix array)\n");
  printf("%s human_chr1\n", [s UTF8String]);
  printf("\nExample (prints the specified suffix)\n");
  printf("%s human_chr1 0 F 1000 50\n", [s UTF8String]);
  printf("\n");
}
