//
//  intersect_mcp.m
//  Ultraconserved
//
// Intersect two mcp arrays.
// Write resulting mcp array to file.
//
//  Created by Scott Christley on 9/22/07.
//  Copyright 2007 Scott Christley. All rights reserved.
//

#import <BioCocoa/BCFoundation.h>

void print_usage(NSArray *args);

int main(int argc, char *argv[])
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  NSArray *args = [[NSProcessInfo processInfo] arguments];

  NSLog(@"%@", [args objectAtIndex: 0]);
  
  if ([args count] != 4) {
    print_usage(args);
    return 1;
  }

  // load MCP files
  NSString *mcpFile1 = [args objectAtIndex: 1];
  BCMCP *mcp1 = [[BCMCP alloc] initWithContentsOfFile: mcpFile1 inMemory: NO];
  if (!mcp1) {
    NSLog(@"Failed to load MCP file: %@\n", mcpFile1);
    return 1;
  }

  NSString *mcpFile2 = [args objectAtIndex: 2];
  BCMCP *mcp2 = [[BCMCP alloc] initWithContentsOfFile: mcpFile2 inMemory: NO];
  if (!mcp2) {
    NSLog(@"Failed to load MCP file: %@\n", mcpFile2);
    return 1;
  }

  if (![mcp1 intersectToFile: [args objectAtIndex: 3] withMCP: mcp2]) {
    NSLog(@"Failed to union MCP files to file: %@\n", [args objectAtIndex: 3]);
    return 1;
  }

  [pool release];
  return 0;
}

void print_usage(NSArray *args)
{
  NSString *s = [[args objectAtIndex: 0] lastPathComponent];

  printf("\nIntersect two mcp files.\n\n");
  printf("Usage:\n");
  printf("%s input_file1 input_file2 output_file\n\n", [s UTF8String]);
  printf("input_file1 : first mcp file, assumes .mcp extension.\n");
  printf("input_file2 : second mcp file, assumes .mcp extension.\n");
  printf("output_file : output mcp file, assumes .mcp extension.\n");
  printf("Example:\n");
  printf("%s human_mouse human_rat human_mouse_rat 40\n\n", [s UTF8String]);
}
