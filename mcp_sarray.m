//
//  mcp_sarray.m
//  Ultraconserved
//
//  Created by Scott Christley on 9/7/07.
//  Copyright 2007 Scott Christley. All rights reserved.
//

#import <BioCocoa/BCFoundation.h>

void print_usage(NSArray *args);

int main(int argc, char *argv[])
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  NSArray *args = [[NSProcessInfo processInfo] arguments];

  // process arguments
  if ([args count] != 6) {
    print_usage(args);
    return 1;
  }

  BOOL isNucleotide;
  if ([[args objectAtIndex: 1] caseInsensitiveCompare: @"N"] == NSOrderedSame)
    isNucleotide = YES;
  else if ([[args objectAtIndex: 1] caseInsensitiveCompare: @"P"] == NSOrderedSame)
    isNucleotide = NO;
  else {
    printf("\nERROR: invalid argument, specify N or P for nucleotide(N) or protein(P).\n");
    print_usage(args);
    return 1;
  }

  int lowerBound = [[args objectAtIndex: 5] intValue];

  // open first suffix array
  NSString *file1 = [args objectAtIndex: 2];
  BCSuffixArray *sArray1 = [[BCSuffixArray alloc] initWithContentsOfFile: file1 inMemory: NO];
  if (!sArray1) {
    NSLog(@"Could not load suffix array from file: %@\n", file1);
    return 1;
  }

  // open second suffix array
  NSString *file2 = [args objectAtIndex: 3];
  BCSuffixArray *sArray2 = [[BCSuffixArray alloc] initWithContentsOfFile: file2 inMemory: NO];
  if (!sArray2) {
    NSLog(@"Could not load suffix array from file: %@\n", file2);
    return 1;
  }

  // MCP calculation
  NSString *outFile = [args objectAtIndex: 4];
  if (![BCMCP mcpToFile: outFile suffixArray: sArray1 withSuffixArray: sArray2 lowerBound: lowerBound]) {
    NSLog(@"Failed to produce MCP file.\n");
    return 1;
  }

  [pool release];
  return 0;
}

void print_usage(NSArray *args)
{
  NSString *s = [[args objectAtIndex: 0] lastPathComponent];

  printf("\nDetermine maximal common prefixes for two suffix arrays.\n\n");
  printf("Usage:\n");
  printf("%s N|P input_file1 input_file2 output_file lower_bound\n\n", [s UTF8String]);
  printf("N|P         : indicate nucleotide(N) or protein(P) sequences.\n");
  printf("input_file1 : first suffix array file, assumes .sa extension.\n");
  printf("input_file2 : second suffix array file, assumes .sa extension.\n");
  printf("output_file : output mcp file, assumes .mcp extension.\n");
  printf("lower_bound : minimal length of mcp.\n\n");
  printf("Example:\n");
  printf("%s n human_chr1 mouse_chr1 human1_mouse1 40\n\n", [s UTF8String]);
}
