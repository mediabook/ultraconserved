#import <Foundation/Foundation.h>
#import <BioCocoa/BCFoundation.h>
#include <sys/types.h>
#include <sys/sysctl.h>

int main (int argc, const char * argv[]) {
  NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/A.gambiae/agambiae.CHROMOSOMES-PEST.AgamP3.fa";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/agambiae.PEST.2L-UNMASKED.fa";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/agambiae.PEST.2R-UNMASKED.fa";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/agambiae.PEST.3L-UNMASKED.fa";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/agambiae.PEST.3R-UNMASKED.fa";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/agambiae.CHROMOSOME_2R-MASKED.AgamP3.fa";

  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/phumanus.SCAFFOLDS-RAW.PhumU1.fa";

  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test.fa";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test1.fa";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/NC_008235.fasta";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/NC_005353.fasta";

  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/chr21.fa.masked";
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/human_mask/chr1.fa.masked";

  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/PfalciparumGenomic_plasmoDB-5.0.fasta";
  
#if 0
  int mib[2];
  size_t len;
  unsigned int *p;
  
  mib[0] = CTL_HW;
  mib[1] = HW_PHYSMEM;
  sysctl(mib, 2, NULL, &len, NULL, 0);
  p = malloc(len);
  sysctl(mib, 2, p, &len, NULL, 0);
  
  printf("%d %u\n", len, *p);
  unsigned int amt = *p;
  amt = amt - 100000000;
  void *data = malloc(amt);
  if (data) printf("allocated %u\n", amt);

  sysctl(mib, 2, p, &len, NULL, 0);
  printf("%d %u\n", len, *p);
#endif
#if 0
  int      ret;
  size_t   oldlen;
  uint64_t physmem_size;
  oldlen = sizeof(physmem_size);
  ret = sysctlbyname("hw.memsize", &physmem_size, &oldlen, NULL, 0);
  printf("%lu %llu %d\n", oldlen, physmem_size, ret);
#endif

#if 0
  NSString *mvfile = @"/Users/scottc/Projects/tools/bio/BioCocoa/BioCocoa.ultra/trunk/Tests/Test-BCFoundation/DataFiles/test.fa";
  //NSString *mvfile = @"/Users/scottc/Desktop/MVFiles/pBR322";
  //NSString *mvfile = @"/Users/scottc/Desktop/MVFiles/HIV1";
  //NSString *mvfile = @"/Users/scottc/Desktop/MVFiles/UBBYB";
  BCSequenceReader *sequenceReader = [[BCSequenceReader alloc] init];
  BCSequenceArray *sequenceArray = [sequenceReader readFileUsingPath: mvfile format: BCFastaFileFormat];

  printf("%d sequences\n", [sequenceArray count]);
  NSLog(@"%@\n", [[sequenceArray sequenceAtIndex: 0] sequenceString]);
  NSString *s = [[sequenceArray sequenceAtIndex: 0] sequenceString];
  NSRange r = [s rangeOfString: @"MGSSLDDEHILSALLQS"];
  printf("%u %u %d %d\n", r.location, r.length, [s length], [[sequenceArray sequenceAtIndex: 0] length]);
  r = [s rangeOfString: @"ICREHNIDMCQSCF"];
  printf("%u %u\n", r.location, r.length);
#endif

#if 0
  BCSequenceReader *sequenceReader = [[BCSequenceReader alloc] init];
  BCSequenceArray *sequenceArray = [sequenceReader readFileUsingPath: genome];

  printf("%d sequences\n", [sequenceArray count]);

  BCSuffixArray *anArray = [[BCSuffixArray alloc] init];
  BCSequence *aSeq = [sequenceArray sequenceAtIndex: 0];

  //printf("%s\n", [[aSeq sequenceString] cString]);
  printf("%s\n", [[[aSeq annotations] description] cString]);

  [anArray constructFromSequence: aSeq strand: nil];
  printf("%d\n", [anArray numOfSuffixes]);
  [anArray writeToFile: [NSString stringWithFormat: @"%@", [genome stringByDeletingPathExtension]] withMasking: YES];
#endif

#if 0
  BCSuffixArray *anArray = [[BCSuffixArray alloc] init];
  if ([anArray constructFromSequenceFile: genome strand: nil]) {
    [anArray writeToFile: genome withMasking: YES];
    //[anArray dumpSuffixArray];
  }
#endif

#if 0
  BCSuffixArray *anArray = [[BCSuffixArray alloc] initWithContentsOfFile: genome inMemory: NO];
  printf("%d\n", [anArray numOfSuffixes]);
  [anArray dumpSuffixArray];
#endif


#if 0
  NSString *genome1 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test.fa";
  NSString *genome2 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test1.fa";
  BCSuffixArray *anArray1 = [[BCSuffixArray alloc] initWithContentsOfFile: genome1 inMemory: NO];
  BCSuffixArray *anArray2 = [[BCSuffixArray alloc] initWithContentsOfFile: genome2 inMemory: NO];
  [BCMCP mcpToFile: genome1 suffixArray: anArray1 withSuffixArray: anArray2 lowerBound: 40];
#endif

#if 0
  NSString *genome1 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test.fa";
  BCMCP *anMCP = [[BCMCP alloc] initWithContentsOfFile: genome1 inMemory: YES];
  [anMCP dumpMCP];
#endif

#if 0
  //NSString *genome1 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test.fa";
  //NSString *genome2 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test1.fa";
  NSString *genome1 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/NC_008235.fasta";
  NSString *genome2 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/phumanus.SCAFFOLDS-RAW.PhumU1.fa";
  BCSuffixArray *anArray1 = [[BCSuffixArray alloc] initWithContentsOfFile: genome1 inMemory: NO];
  BCSuffixArray *anArray2 = [[BCSuffixArray alloc] initWithContentsOfFile: genome2 inMemory: NO];
  NSMutableArray *sa = [NSMutableArray new];
  [sa addObject: anArray1];
  [sa addObject: anArray2];
  BCSuffixArrayUnionEnumerator *sae = [[BCSuffixArrayUnionEnumerator alloc] initWithSuffixArrays: sa];
  int aPos, aSeq, anArray;
  while ([sae nextSuffixPosition:&aPos sequence:&aSeq suffixArray:&anArray]) {
    BCSuffixArray *anSA = [sa objectAtIndex: anArray];
    printf("array %d : ", anArray);
    [anSA dumpSuffixArrayForSequence:aSeq position:aPos length:50];
  }
#endif
  
#if 0
  //NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test.fa";
  NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test2.fa";
  BCCachedSequenceFile *cacheFile = [BCCachedSequenceFile readCachedFileUsingPath: genome];
  int i, j;
  for (i = 0; i < 517; ++i) {
    if ((i % 78) == 0) printf("\n");
    printf("%c", [cacheFile symbolAtPosition: i forSequenceNumber: 0]);
  }
  printf("\n");
  for (i = (2*517) - 1, j = 0; i >= 517; --i, ++j) {
    if ((j % 78) == 0) printf("\n");
    printf("%c", [cacheFile symbolAtPosition: i forSequenceNumber: 0]);
  }
  printf("\n\n");
  printf("%c\n", [cacheFile symbolAtPosition: 517 forSequenceNumber: 0]);

  for (i = 0; i < 102; ++i) {
    if ((i % 51) == 0) printf("\n");
    printf("%c", [cacheFile symbolAtPosition: i forSequenceNumber: 1]);
  }
  printf("\n");
  for (i = (2*102) - 1, j = 0; i >= 102; --i, ++j) {
    if ((j % 51) == 0) printf("\n");
    printf("%c", [cacheFile symbolAtPosition: i forSequenceNumber: 1]);
  }
  printf("\n");
#endif

#if 1
  NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test2.fa";
  BCCachedSequenceFile *cacheFile = [BCCachedSequenceFile readCachedFileUsingPath: genome];
  char buf[518];
  [cacheFile symbols: buf atPosition: 0 ofLength: 517 forSequenceNumber: 0];
  buf[517] = '\0';
  printf("%s\n\n", buf);
  [cacheFile symbols: buf atPosition: 517 ofLength: 517 forSequenceNumber: 0];
  buf[517] = '\0';
  printf("%s\n\n", buf);
  [cacheFile symbols: buf atPosition: 0 ofLength: 102 forSequenceNumber: 1];
  buf[102] = '\0';
  printf("%s\n\n", buf);
  [cacheFile symbols: buf atPosition: 102 ofLength: 102 forSequenceNumber: 1];
  buf[102] = '\0';
  printf("%s\n\n", buf);
#endif

#if 0
  NSString *genome = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/test2.fa";
  BCCachedSequenceFile *cacheFile = [BCCachedSequenceFile readCachedFileUsingPath: genome];
  NSString *seqID = @"1103172107370 /type=bodylouse_scaffold";
  int i;
  for (i = 0; i < 517; ++i) {
    if ((i % 50) == 0) printf("\n");
    printf("%c", [cacheFile symbolAtPosition: i forSequence: seqID]);
  }
  printf("\n");
#endif

#if 0
  NSString *genome1 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/NC_008235.fasta";
  NSString *genome2 = @"/Users/scottc/Projects/research/vectorbase/data/genomes/test/NC_005353.fasta";
  BCCachedSequenceFile *cacheFile1 = [BCCachedSequenceFile readCachedFileUsingPath: genome1];
  BCCachedSequenceFile *cacheFile2 = [BCCachedSequenceFile readCachedFileUsingPath: genome2];
  int i;
  for (i = 132842; i < (132842 + 66); ++i) {
    printf("%c", [cacheFile1 symbolAtPosition: i forSequenceNumber: 0]);
  }
  printf("\n");
  for (i = 363347; i < (363347 + 66); ++i) {
    printf("%c", [cacheFile2 symbolAtPosition: i forSequenceNumber: 0]);
  }
  printf("\n");
#endif

  NSLog(@"done\n");
  [pool release];
  return 0;
}
