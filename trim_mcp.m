//
//  trim_mcp.m
//  Ultraconserved
//
//  Created by Scott Christley on 10/17/07.
//  Copyright 2007 Scott Christley. All rights reserved.
//

#import <BioCocoa/BCFoundation.h>

void print_usage(NSArray *args);

int main(int argc, char *argv[])
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  NSArray *args = [[NSProcessInfo processInfo] arguments];
  
  if ([args count] != 3) {
    print_usage(args);
    return 1;
  }

  // load MCP file
  NSString *mcpFile1 = [args objectAtIndex: 1];
  BCMCP *mcp1 = [[BCMCP alloc] initWithContentsOfFile: mcpFile1 inMemory: NO];
  if (!mcp1) {
    NSLog(@"Failed to load MCP file: %@\n", mcpFile1);
    return 1;
  }

  // trim MCP
  if (![mcp1 trimToFile: [args objectAtIndex: 2]]) {
    NSLog(@"Failed to trim MCP to file: %@\n", [args objectAtIndex: 2]);
    return 1;
  }

  [pool release];
  return 0;
}

void print_usage(NSArray *args)
{
  NSString *s = [[args objectAtIndex: 0] lastPathComponent];

  printf("\nTrim mcp file of overlaps.\n\n");
  printf("Usage:\n");
  printf("%s input_file output_file\n\n", [s UTF8String]);
  printf("input_file  : input mcp file, assumes .mcp extension.\n");
  printf("output_file : output mcp file, assumes .mcp extension.\n");
  printf("Example:\n");
  printf("%s human_mouse_rat hmr_trim\n\n", [s UTF8String]);
}
